using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Template
{
	class MyApplication
	{
		// member variables
		public Surface screen;
		private int cartesianRange = 4;
		private double angleProgression = 0;
		Surface map;
		float[,] h;
		// initialize
		public void Init()
		{
			map = new Surface("../../assets/coin.png");
			h = new float[256, 256];
			for (int y = 0; y < 256; y++) for (int x = 0; x < 256; x++)
					h[x, y] = ((float)(map.pixels[x + y * 256] & 255)) / 256;
		}
		// tick: renders one frame
		public void Tick()
		{
			screen.Clear(0);
			float x1 = -1, y1 = 0.5f;
			float x2 = 1, y2 = 0.5f;
			float x3 = 1, y3 = -0.5f;
			float x4 = -1, y4 = -0.5f;
			
			float rx1 = (float)(x1 * Math.Cos(angleProgression) - y1 * Math.Sin(angleProgression));
			float ry1 = (float)(x1 * Math.Sin(angleProgression) + y1 * Math.Cos(angleProgression));
			float rx2 = (float)(x2 * Math.Cos(angleProgression) - y2 * Math.Sin(angleProgression));
			float ry2 = (float)(x2 * Math.Sin(angleProgression) + y2 * Math.Cos(angleProgression));
			float rx3 = (float)(x3 * Math.Cos(angleProgression) - y3 * Math.Sin(angleProgression));
			float ry3 = (float)(x3 * Math.Sin(angleProgression) + y3 * Math.Cos(angleProgression));
			float rx4 = (float)(x4 * Math.Cos(angleProgression) - y4 * Math.Sin(angleProgression));
			float ry4 = (float)(x4 * Math.Sin(angleProgression) + y4 * Math.Cos(angleProgression));


			screen.Box(TX(-1), TY(1), TX(1), TY(-1), 0xff0000);
			screen.Line(TX(rx1), TY(ry1), TX(rx2), TY(ry2), 0x00ff00);
			screen.Line(TX(rx2), TY(ry2), TX(rx3), TY(ry3), 0x00ff00);
			screen.Line(TX(rx3), TY(ry3), TX(rx4), TY(ry4), 0x00ff00);
			screen.Line(TX(rx4), TY(ry4), TX(rx1), TY(ry1), 0x00ff00);

			angleProgression += (Math.PI / 1000) % 2 * Math.PI;
		}

		private int MixColor(int red, int green, int blue)
		{
			return (red << 16) + (green << 8) + blue;
		}

		public void RenderGL()
        {
			var M = Matrix4.CreatePerspectiveFieldOfView(1.6f, 1.3f, .1f, 1000);
			GL.LoadMatrix(ref M);
			GL.Translate(0, 0, -1);
			GL.Rotate(110, 1, 0, 0);
			GL.Rotate(angleProgression * 180 / 3.141593, 0, 0, 1);
			GL.Color3(1.0f, 0.0f, 0.0f);
			GL.Begin(PrimitiveType.Triangles);
			GL.Vertex3(-0.5f, -0.5f, 0);
			GL.Vertex3(0.5f, -0.5f, 0);
			GL.Vertex3(-0.5f, 0.5f, 0);
			GL.End();
		}

		private int TX(float cartesianX)
        {
			cartesianX += cartesianRange / 2;
			cartesianX = cartesianX / cartesianRange
				* screen.minScreenRange
				+ (screen.maxScreenRange - screen.minScreenRange) / 2;
			return (int)cartesianX;
        }

		private int TY(float cartesianY)
        {
			cartesianY += cartesianRange / 2;
			cartesianY = cartesianRange - cartesianY;
			cartesianY = cartesianY / cartesianRange * screen.minScreenRange;
			// TODO fix y-center-shift
			return (int)cartesianY;
        }
	}
}